function(generate_spirv_shaders target_name)
	get_target_property(sources ${target_name} SOURCES)
	foreach(source ${sources})
		cmake_path(GET source EXTENSION LAST_ONLY extension)
		if(extension)
			string(TOLOWER ${extension} extension_lowercase)
			if(${extension_lowercase} STREQUAL ".glsl")
				source_group(Shaders FILES ${source})
				cmake_path(GET source STEM LAST_ONLY filename)
				set(SPIRV "${PROJECT_BINARY_DIR}/shaders/${filename}.spv")

				list(APPEND glsl_shaders ${source})
				list(APPEND spirv_shaders ${SPIRV})

				add_custom_command(
					OUTPUT ${SPIRV}
					COMMAND ${CMAKE_COMMAND} -E make_directory ${PROJECT_BINARY_DIR}/shaders
					COMMAND ${Vulkan_GLSLANG_VALIDATOR_EXECUTABLE} -V ${source} -o ${SPIRV}
					WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
					DEPENDS ${source})
			endif()
		endif()
	endforeach()

	add_custom_target(
		generate_shaders_${target_name}
		ALL
		SOURCES ${glsl_shaders}
		DEPENDS ${spirv_shaders})

	add_dependencies(${target_name} generate_shaders_${target_name})
endfunction()
